var Main = function(game){

};

Main.prototype = {

	create: function() {

		var me = this;

		// Jarak untuk platform awal
		me.spacing = 260; //300

		//etapkan skor awal
		me.score = 0;

		//Buat dimensi ubin yang kita gunakan
		me.tileWidth = me.game.cache.getImage('tile').width;
		me.tileHeight = me.game.cache.getImage('tile').height;

		//Tetapkan warna latar ke (biru)
		me.game.stage.backgroundColor = '1E90FF';

		//Aktifkan sistem fisika Arcade
		me.game.physics.startSystem(Phaser.Physics.ARCADE);

		//Tambahkan grup platform untuk menampung semua ubin kami, dan buatlah banyak dari mereka
		me.platforms = me.game.add.group();
		me.platforms.enableBody = true;
		me.platforms.createMultiple(250, 'tile');

		//Buat inital di layar platform
		me.initPlatforms();

		//Tambahkan pemain ke layar
		me.createPlayer();

		//Buat skor
		me.createScore();

		//Tambahkan sebuah platform setiap 2.5 detik
		me.timer = game.time.events.loop(2500, me.addPlatform, me);

	    //Aktifkan tombol kursor agar kita bisa membuat beberapa kontrol
	    me.cursors = me.game.input.keyboard.createCursorKeys();  


	},

	update: function() {
		var me = this;

		//Buatlah sprite bertabrakan dengan lapisan tanah
		me.game.physics.arcade.collide(me.player, me.platforms);

		//Buat sprite melompat saat tombol atas ditekan
	    if(me.cursors.up.isDown && me.player.body.wasTouching.down) {
	     	me.player.body.velocity.y = -1400;
	    }
	    //Buat player belok ke kiri
	    if(me.cursors.left.isDown){
	    	me.player.body.velocity.x += -30;
	    }
	    //Buat player belok ke kanan
	    if(me.cursors.right.isDown){
	    	me.player.body.velocity.x += 30;
	    }
		
	    //periksa apakah player jatuh
	    if(me.player.body.position.y >= me.game.world.height - me.player.body.height){
	    	me.gameOver();
	    }
	},

	gameOver: function(){
		this.game.state.start('Main');
	},

	addTile: function(x, y){

		var me = this;

		//Buat ubin yang saat ini tidak ada di layar
	    var tile = me.platforms.getFirstDead();

	    //Reset ke koordinat yang ditentukan
	    tile.reset(x, y);
	    tile.body.velocity.y = 100; //150
	    tile.body.immovable = true;

	    //Saat ubin meninggalkan layar maka hancur/hilang
	    tile.checkWorldBounds = true;
	    tile.outOfBoundsKill = true;	
	},

	addPlatform: function(y){

		var me = this;

		//Jika tidak ada posisi y yang dipasok, buatlah tepat di luar layar
		if(typeof(y) == "undefined"){
			y = -me.tileHeight;
			//Tingkatkan skor pemain
			me.incrementScore();
		}

		//Buat berapa banyak ubin yang perlu dipasang di seluruh layar
		var tilesNeeded = Math.ceil(me.game.world.width / me.tileWidth);

		//Tambahkan lubang secara acak
	    var hole = Math.floor(Math.random() * (tilesNeeded - 3)) + 1;
		

	    // Terus ciptakan ubin di samping satu sama lain sampai kami memiliki keseluruhan baris
		// Jangan menambahkan ubin tempat lubang acak itu
	    for (var i = 0; i < tilesNeeded; i++){
	        if (i != hole && i != hole + 1){
	        	this.addTile(i * me.tileWidth, y); 
	        } 	    	
	    }

	},

	initPlatforms: function(){

		var me = this,
			bottom = me.game.world.height - me.tileHeight,
			top = me.tileHeight;

		//Terus membuat platform sampai mereka mencapai (dekat) bagian atas layar
		for(var y = bottom; y > top - me.tileHeight; y = y - me.spacing){
			me.addPlatform(y);
		}

	},

	createPlayer: function(){

		var me = this;

		// Tambahkan player ke permainan dengan membuat sprite baru
		me.player = me.game.add.sprite(me.game.world.centerX, me.game.world.height - (me.spacing * 2 + (3 * me.tileHeight)), 'player');
		// Tetapkan titik awal player untuk berada di tengah horizontal
		me.player.anchor.setTo(0.5, 1.0);
		// Aktifkan fisika pada player
		me.game.physics.arcade.enable(me.player);
		// Jadikan player jatuh dengan menerapkan gravitasi
		me.player.body.gravity.y = 2000;
		// Buat player bertabrakan dengan batas
		me.player.body.collideWorldBounds = true;
		// Buat pemain itu terpental sedikit
		me.player.body.bounce.y = 0.1;

	},

	createScore: function(){

		var me = this;

		var scoreFont = "100px Arial";

		me.scoreLabel = me.game.add.text((me.game.world.centerX), 100, "0", {font: scoreFont, fill: "#fff"}); 
		me.scoreLabel.anchor.setTo(0.5, 0.5);
		me.scoreLabel.align = 'center';

	},

	incrementScore: function(){

		var me = this;

		me.score += 1;   
		me.scoreLabel.text = me.score; 		

	},

};